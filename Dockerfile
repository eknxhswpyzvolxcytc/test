FROM --platform=linux/arm/v6 alpine:latest
RUN apk update
RUN apk add wget curl nano vim aria2
RUN uname -m